﻿using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace BFC
{

    internal class Program
    {

        private static readonly object _lock = new object();


        static int Main(string[] args)
        {

            string appPath = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

            //no args, quit.
            if (args.Length == 0)
            {
                ShowUsage(true);
                return 0;
            }

            //parse arguments
            Arguments p;
            try
            {
                p = new Arguments(args, appPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine();
                Console.WriteLine($"*** ERROR: {ex.Message}");
                Console.WriteLine();
                ShowUsage(false);
                return -1;
            }


            var cts = new CancellationTokenSource();

            // Run a task so that we can cancel from another thread.
            Task.Factory.StartNew(() =>
            {
                if (Console.ReadKey(true).Key == ConsoleKey.Escape) cts.Cancel();
            });


            Console.WriteLine();

            var c = new Collector(p.Url, p.First, p.Last, p.SearchExpression, StringComparison.CurrentCultureIgnoreCase, -1, cts);
            c.ItemProcessed += OnItemProcessed;
            c.Cancelled += OnCancelled;

            c.Start();

            Collector.SaveUrlList(c.Success, $@"{p.OutputFolder}BFC_{p.First}-{p.Last}_Success ({c.Success.Count}).txt");
            Collector.SaveUrlList(c.NoMatch, $@"{p.OutputFolder}BFC_{p.First}-{p.Last}_NoMatch ({c.NoMatch.Count}).txt");
            Collector.SaveUrlList(c.Failure, $@"{p.OutputFolder}BFC_{p.First}-{p.Last}_Failure ({c.Failure.Count}).txt");
            Console.WriteLine();
            Console.WriteLine($"  Total time:      {c.TotalTime.ToString()}");
            Console.WriteLine($"  Items processed: {c.ItemsProcessed}/{c.ItemsTotal}");
            Console.WriteLine();
            Console.WriteLine($"  Output files were stored to {p.OutputFolder}");

            return 0;
        }


        /// <summary>
        /// Update UI on item processed.
        /// </summary>
        private static void OnItemProcessed(object sender, ItemProcessedEventArgs e)
        {
            var status = new[] { "  [     ]", "  [FOUND]", "  [ERROR]" };
            var highlight = new[] { ConsoleColor.Gray, ConsoleColor.Green, ConsoleColor.Red };

            byte idx = (byte)e.Result;
            string s = status[idx];

            // Solves f*ed up colors when running in Visual Studio. (Never happened to me outside of it)
            lock (_lock)
            {

                Console.ForegroundColor = highlight[idx];
                Console.Write($"{s}");

                var left = Console.CursorLeft;
                var top = Console.CursorTop;

                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.Write($"  {e.ProgressPercentage}");

                Console.ForegroundColor = ConsoleColor.Gray;
                Console.Write($"   {e.Item.Url}");

                Console.WriteLine();

            }

        }


        private static void OnCancelled(object sender, EventArgs e)
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("  Operation cancelled by user!");
            Console.ForegroundColor = ConsoleColor.DarkGray;
        }


        /// <summary>
        /// 
        /// </summary>
        private static void ShowUsage(bool includeHeader)
        {
            var appFile = System.IO.Path.GetFileName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
            var appVer = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            AssemblyCopyrightAttribute appC = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false)[0] as AssemblyCopyrightAttribute;

            if (includeHeader)
            {
                Console.WriteLine();
                Console.WriteLine($"BFC {appVer} {appC?.Copyright}");
                Console.WriteLine();

                Console.WriteLine("  Downloads URLs in the specified range and generates 3 output files containing:");
                Console.WriteLine("  - URLs that contain the search expression.");
                Console.WriteLine("  - URLs that don't.");
                Console.WriteLine("  - URLs that failed to download for any reason.");
                Console.WriteLine();
                Console.WriteLine("   Use {0} placeholder to specify the numerical portion of the Url.");
                Console.WriteLine("   Press [Esc] to cancel.");
                Console.WriteLine();

            }

            Console.WriteLine($"  Usage:   {appFile} <url> <firstIndex> <lastIndex> <searchExpression> [<outputFolder>]");
            Console.WriteLine(@"  Example: " + appFile + " http://birdz.sk/forum/x/{0}-tema.html 1003 1010 Whatever C:\\Wherever");

        }



    }
}
