﻿using System;

namespace BFC
{

    internal class ItemProcessedEventArgs : EventArgs
    {

        public readonly PageItem Item;
        public readonly ItemResult Result;
        public readonly string ProgressPercentage;

        //TODO: Add Bypass argument to allow excluding items by custom logic in the event handler

        public ItemProcessedEventArgs(PageItem item, ItemResult result, string progressPercentage)
        {
            Item = item;
            Result = result;
            ProgressPercentage = progressPercentage;
        }

    }
}
