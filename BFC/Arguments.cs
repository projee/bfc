﻿using System;

namespace BFC
{

    /// <summary>
    /// Command-line arguments processor
    /// </summary>
    internal class Arguments
    {

        public string Url { get; set; }
        public int First { get; set; }
        public int Last { get; set; }
        public string SearchExpression { get; set; }
        public string OutputFolder { get; set; }


        public Arguments(string[] args, string defaultOutputFolder)
        {
            Initialize(args, defaultOutputFolder);
        }


        private void Initialize(string[] args, string defaultOutputFolder)
        {

            if (args.Length < 4 || args.Length > 5) throw new Exception("Invalid number of arguments.");


            if (!args[0].Contains(@"{0}")) throw new Exception(@"Invalid Url - {0} placeholder missing.");
            if (!Uri.TryCreate(args[0], UriKind.Absolute, out Uri url)) throw new Exception("Invalid Url.");
            Url = url.ToString();

            if (!int.TryParse(args[1], out int first)) throw new Exception("Invalid argument \"firstIndex\".");
            First = Math.Abs(first);

            if (!int.TryParse(args[2], out int last)) throw new Exception("Invalid argument \"lastIndex\".");
            Last = Math.Abs(last);

            //Swap indexes if the last is smaller than the first.
            if (Last < First) (First, Last) = (Last, First);
            
            SearchExpression = args[3];

            //Output folder is optional. App folder is used is none specified.
            string folder = args.Length == 5 ? args[4] : "";

            if (string.IsNullOrWhiteSpace(folder))
            {
                if (string.IsNullOrWhiteSpace(defaultOutputFolder)) throw new Exception("Output folder has to be specified.");
                OutputFolder = defaultOutputFolder;
            }
            else
            {
                if (!System.IO.Directory.Exists(folder)) throw new Exception($"The specified output folder \"{folder}\" does not exist.");
                OutputFolder = folder;
            }

            if (!OutputFolder.EndsWith(@"\")) OutputFolder += @"\";

        }

    }
}
