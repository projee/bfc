﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("BFC")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("BFC")]
[assembly: AssemblyCopyright("Copyright (c) 2019 Brazzers, Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("569e5b65-d4c1-43d7-a818-0c47bca5aa44")]
[assembly: AssemblyVersion("19.1.472.0112")]
[assembly: AssemblyFileVersion("19.1.472.0112")]
