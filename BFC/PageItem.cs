﻿
namespace BFC
{
    /// <summary>
    /// Represents a single page.
    /// </summary>
    internal class PageItem
    {

        public int Index { get; set; }
        public string Url { get; set; }
        public string Content { get; set; }


        public PageItem() { }

        public PageItem(string url) => Url = url;

        public PageItem(string url, int index) : this(url) => Index = index;

        public PageItem(string url, string content) : this(url) => Content = content;

        public PageItem(string url, int index, string content) : this(url, content) => Index = index;

    }
}
