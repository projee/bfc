﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BFC
{

    /// <summary>
    /// The mighty page collector class.
    /// </summary>
    internal class Collector
    {

        private readonly string _url;

        private long _total = 0;
        private long _current = 0;
        private int _maxDegreeOfParallelism = -1;
        private StringComparison _comparisonType;
        private CancellationTokenSource _cts;
        private string _searchExpression;
        private int _first;
        private int _last;

        private TimeSpan _totalTime = new TimeSpan();

        /// <summary>
        /// Gets total processing time.
        /// </summary>
        public TimeSpan TotalTime => _totalTime;


        /// <summary>
        /// Gets the number of processed items.
        /// </summary>
        public long ItemsProcessed => Interlocked.Read(ref _current);


        /// <summary>
        /// Gets the total number of items.
        /// </summary>
        public long ItemsTotal => _total;


        /// <summary>Collection of pages with content that contains the search expression.</summary>
        public ConcurrentBag<PageItem> Success { get; } = new ConcurrentBag<PageItem>();


        /// <summary>Collection of pages that DON'T contain the search expression.</summary>
        public ConcurrentBag<PageItem> NoMatch { get; } = new ConcurrentBag<PageItem>();


        /// <summary>Collection of pages that failed to download for any reason.</summary>
        public ConcurrentBag<PageItem> Failure { get; } = new ConcurrentBag<PageItem>();


        //Event raised when a page is processed.
        public event EventHandler<ItemProcessedEventArgs> ItemProcessed;
        protected virtual void OnItemProcessed(ItemProcessedEventArgs e) => ItemProcessed?.Invoke(this, e);


        //Event raised when the operation is cancelled by user.
        public event EventHandler<EventArgs> Cancelled;
        protected virtual void OnCancelled(EventArgs e) => Cancelled?.Invoke(this, e);


        // hide parameterless constructor.
        private Collector() { }


        /// <summary>
        /// Returns instance of the <see cref="Collector"/> class.
        /// </summary>
        /// <param name="url">Url of page to be downloaded. Use {0} placeholder to specify the numerical portion of the Url to be replaced on each iteration.</param>
        /// <param name="first">Index of the 1st page.</param>
        /// <param name="last">Index of the last page.</param>
        /// <param name="searchExpression">String to be found.</param>
        /// <param name="comparisonType">One of the enumeration values that specifies the rules for the search.</param>
        /// <param name="maxDegreeOfParallelism">Maximum number of concurrent tasks.</param>
        /// <param name="cts">Cancellation token.</param>
        public Collector(string url, int first, int last, string searchExpression, StringComparison comparisonType = StringComparison.CurrentCultureIgnoreCase, int maxDegreeOfParallelism = -1, CancellationTokenSource cts = null)
        {
            _url = url;
            _first = first;
            _last = last;
            _total = last - first + 1;
            _current = 0;

            _searchExpression = searchExpression;
            _comparisonType = comparisonType;
            _maxDegreeOfParallelism = maxDegreeOfParallelism;
            _comparisonType = comparisonType;
            _cts = cts;

        }


        /// <summary>
        /// Downloads and stores page Urls to collections based on the search expression occurence.
        /// </summary>
        public void Start()
        {

            _current = 0;
            _totalTime = TimeSpan.Zero;

            var sw = new Stopwatch();
            sw.Start();

            var opt = new ParallelOptions() { MaxDegreeOfParallelism = _maxDegreeOfParallelism };
            if (_cts != null) opt.CancellationToken = _cts.Token;

            try
            {

                Parallel.For(_first, _last + 1, opt, index =>
                {

                    opt.CancellationToken.ThrowIfCancellationRequested();

                    string url = string.Format(_url, index);

                    using (var client = WebClient)
                    {
                        try
                        {

                            string data = client.DownloadString(url);

                            //TODO: Add RegEx support

                            if (data.IndexOf(_searchExpression, _comparisonType) > -1)
                            {
                                var item = new PageItem(url, index);
                                Success.Add(item);
                                IncrementProgressCounter();
                                OnItemProcessed(new ItemProcessedEventArgs(item, ItemResult.Success, GetProgressPercentage()));
                            }
                            else
                            {
                                var item = new PageItem(url, index);
                                NoMatch.Add(item);
                                IncrementProgressCounter();
                                OnItemProcessed(new ItemProcessedEventArgs(item, ItemResult.NoMatch, GetProgressPercentage()));
                            }
                        }
                        catch
                        {
                            var item = new PageItem(url, index);
                            Failure.Add(item);
                            IncrementProgressCounter();
                            OnItemProcessed(new ItemProcessedEventArgs(item, ItemResult.Failure, GetProgressPercentage()));
                        }

                    }

                });

            }
            catch (OperationCanceledException e)
            {
                OnCancelled(new EventArgs());
            }
            finally
            {
                _cts?.Dispose();

                sw.Stop();
                _totalTime = sw.Elapsed;
            }

        }


        /// <summary>
        /// Returns instance of WebClient. May be overriden in a test method to provide a fake instance.
        /// </summary>
        protected virtual IWebClient WebClient => new SystemNetWebClientEx();


        /// <summary>
        /// This class only exists to be able to return <see cref="System.Net.WebClient"/> as <see cref="IWebClient"/> in the <see cref="Collector.WebClient"/> method, 
        /// because the <see cref="System.Net.WebClient"/> class does not explicitly implement our <see cref="IWebClient"/> interface.
        /// </summary>
        private class SystemNetWebClientEx : System.Net.WebClient, IWebClient { }


        /// <summary>
        /// Stores a list of Urls into a file.
        /// </summary>
        /// <param name="collection">Source collection.</param>
        /// <param name="path">The file to write to.</param>
        public static void SaveUrlList(ConcurrentBag<PageItem> collection, string path)
        {
            var ordered = collection.OrderBy(x => x.Index);

            var sb = new System.Text.StringBuilder();

            foreach (PageItem item in ordered)
            {
                sb.AppendLine(item.Url);
            }

            System.IO.File.WriteAllText(path, sb.ToString());
        }


        /// <summary>
        /// Stores content of all pages to disk.
        /// </summary>
        /// <param name="collection">Source collection.</param>
        /// <param name="folder">Output folder.</param>
        public static void SaveUrlContent(ConcurrentBag<PageItem> collection, string folder)
        {
            foreach (PageItem item in collection)
            {
                string fileName = $"{folder}{item.Index}.htm";
                System.IO.File.WriteAllText(fileName, item.Content, System.Text.Encoding.Default);
            }
        }


        /// <summary>
        /// Increments the progress counter.
        /// </summary>
        private void IncrementProgressCounter() => Interlocked.Increment(ref _current);


        /// <summary>
        /// Gets current progress percentage string.
        /// </summary>
        private string GetProgressPercentage()
        {
            long c = Interlocked.Read(ref _current);
            double p = ((double)c / _total);
            return $"{p.ToString("0.00%", CultureInfo.InvariantCulture),7}"; //left padding
        }



    }
}



