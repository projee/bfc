﻿using System;
using System.Threading.Tasks;

namespace BFC
{

    internal interface IWebClient : IDisposable
    {
        Task<string> DownloadStringTaskAsync(string address);
        string DownloadString(string address);
    }

}
